
import orm = require( 'orm' );

// NOTE - Order is important here
var modelFiles: string[] = [
    // Top-level tables
    './account',
    './goal',
    './group',
    './accountSettings',
    './goalSettings',
    './message',
    './privateMessage',
    './notificationSettings',
    './session',
];

function loadAllModels( db: orm.ORM, cb: Function ) {

    for ( var i in modelFiles ) {
        var modelFile: string = modelFiles[i];
        console.info( "...Loading model: " + modelFile );
        db.load( modelFile, ( err: Error ) => {
            if ( err ) {
                console.error( "Error loading model: " + modelFile + " Error: " + err );
                return cb( err );
            }
        });
    }

    // Success
    return cb();
}

export = loadAllModels;