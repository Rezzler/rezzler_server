/// <reference path="../references.ts" />

import orm = require('orm');

import GroupCommon = require('../classes/groups/GroupCommon');

/*
 *
 */
function defineGroupModel(db : orm.ORM, cb : Function) {
    var group : orm.Model<GroupCommon.IGroupBase, GroupCommon.IGroupInstance> = db.define('group', {
        name : String,
        createdDate: {type: 'date', time:true},
        isPublic : Boolean,
        latitude : Number,
        longitude : Number

    });


    if(db.models['goal'])
    {
        console.info("db.models.goal is defined...");
    }
   
    //--------------------------------------------
    // Define Group->Goals (default, 1 to many)
    //--------------------------------------------
    group.hasMany('default_goals', db.models['goal'], { 
        addedDate: Date,
        isActive: Boolean
    },{
        reverse: 'defaultGoals',
        autoFetch: true
    });

    group.hasMany('members', db.models['account'], {
       joinDate: Date,
       role: String
    }, {
        reverse: 'groups',
        autoFetch: true
    });

    return cb();
};

export = defineGroupModel;
