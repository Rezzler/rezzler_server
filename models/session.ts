/// <reference path="../references.ts" />

import orm = require('orm');
import moment = require('moment');
import SessionCommon = require('../classes/sessions/SessionCommon');

/*
 *
 */
function defineSessionModel(db : orm.ORM, cb : Function) {
    var gpsMarker : orm.Model<SessionCommon.IGPSMarker, SessionCommon.IGPSMarkerInstance> = db.define('gpsmarker', {
        timestamp : {type: 'date', time:true},
        latitude : Number,
        longitude : Number
    });
    
    var session : orm.Model<SessionCommon.ISession, SessionCommon.ISessionInstance> = db.define('session', {
        name : String,
        sessionType : Number,
        createdDate: {type: 'date', time:true},
        stoppedDate: {type: 'date', time:true},
        latitude : Number,
        longitude: Number,
        processed : Boolean,
        totalSteps: Number,
        totalDistance: Number,
    }, {
        methods: {
            createdDateCalendarString: function() {
                return moment(this.createdDate).calendar();
            }
        }
    });

    gpsMarker.hasOne('session', session, { 
    },{
        reverse: 'gpsMarkers',
        autoFetch: true
    });
    
    session.hasOne('account', db.models['account'], {
           
    }, {
       reverse: 'sessions',
       autoFetch: false,
    });

    return cb();
};

export = defineSessionModel;
