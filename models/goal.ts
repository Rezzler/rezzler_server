/*
 *
 */
 /// <reference path="../references.ts" />
import orm = require('orm');


function defineGoalModel (db : orm.ORM, cb : Function) {
    var goal = db.define('goal', {
        type : Number,
        createdDate : Date,
        
        freqUnit : Number,
        freqCount : Number,
        
        startDate : Date,
        isActive : Boolean
        
    }, {
        methods: {
            getName : function() {
                var time : string;
                
                switch(this.freqUnit) 
                {
                    case GoalEnums.GoalFrequency.Daily:
                        time = "Day";
                        break;
                    case GoalEnums.GoalFrequency.Weekly:
                        time = "Week";
                        break;
                    case GoalEnums.GoalFrequency.Monthly:
                        time = "Month";
                        break;
                    case GoalEnums.GoalFrequency.Yearly:
                        time = "Year";
                        break;
                }
                
                switch(this.type) {
                    case GoalEnums.GoalType.Steps:
                        return "Take " + this.freqCount + " Steps Per " + time;
                    case GoalEnums.GoalType.CheckIn:
                        return "Check-in " + this.freqCount + " Times Per " + time;
                    case GoalEnums.GoalType.FootDistance:
                        return "Walk or Run " + this.freqCount + " Miles Per " + time;
                    case GoalEnums.GoalType.BikingDistance:
                        return "Bike " + this.freqCount + " Miles Per " + time;
                    default:
                        return "blah";
                }
            }
        }
    });
    
    
    //--------------------------------------------
    // Define Account->Goals (default, 1 to many)
    //--------------------------------------------
    db.models['account'].hasMany('goals', goal, { 
        addedDate: Date,
    },{
        reverse: 'owner',
        autoFetch: true
    });


    return cb();
};

export = defineGoalModel;