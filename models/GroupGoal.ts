import orm = require('orm');

function defineAccountSettingsModel(db : orm.ORM, cb : Function) {
    var AccountSettings = db.define('privateMessage', {
      language : String,
      body : String,
      visibility : String,
      Anonomized : Boolean,
    });

    /*AccountSettings.hasMany('family', db.models['account'], {
        type : String //brother,sister, father, mother ect.
    }, {
        reverse: 'accountSettings',
        autoFetch: true
    });

    AccountSettings.hasMany('accountabilityPartners', db.models['account'], {

    }, {
        reverse: 'accountSettings',
        autoFetch: true
    });*/

    AccountSettings.hasOne('goalSettings', db.models['goalSettings'], {

    }, {
        reverse: 'accountSettings',
        autoFetch: true
    });

    AccountSettings.hasOne('notificationSettings', db.models['notificationSettings'], {

    }, {
        reverse: 'accountSettings',
        autoFetch: true
    });

    return cb();
}

export = defineAccountSettingsModel;
