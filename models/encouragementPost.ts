﻿/*
 *
 */
import orm = require('orm');


function defineEncouragementPostModel(db: orm.ORM, cb: Function) {
    var encouragementPost = db.define('encoragementPost', {
        message: String,
        createdDate: Date,
    });
    

    //--------------------------------------------
    // 
    //--------------------------------------------
    encouragementPost.hasOne('goal', db.models['goal'], {
    }, {
            reverse: 'encoragementPost',
            autoFetch: true
        });

    
    //--------------------------------------------
    // 
    //--------------------------------------------
    encouragementPost.hasOne('sender', db.models["account"], {
    }, {
            reverse: 'encoragementPosts',
            autoFetch: true
        });


    return cb();
};

export = defineEncouragementPostModel;