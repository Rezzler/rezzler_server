import orm = require('orm');

function defineMessageModel(db : orm.ORM, cb : Function) {
    var Message = db.define('message', {
      Title : String,
      body : String
    });

    Message.hasMany('taggedUsers', db.models['account'], {
       LatestMessageDate: Date,
       role: String
    }, {
        reverse: 'messagesByTag',
        autoFetch: true
    });

    Message.hasMany('taggedGoals', db.models['goal'], {
       LatestMessageDate: Date,
       role: String
    }, {
        reverse: 'messagesByTag',
        autoFetch: true
    });

    return cb();
}

export = defineMessageModel;
