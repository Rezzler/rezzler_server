import orm = require('orm');

function definePrivateMessageModel(db : orm.ORM, cb : Function) {
    var privateMessage = db.define('privateMessage', {
        body : String
    });

    privateMessage.hasMany('members', db.models['account'], {
       LatestMessageDate : Date,
       read : Boolean,
       role : String
    }, {
        reverse : 'privateMessages',
        autoFetch : true
    });

    privateMessage.hasMany('taggedUsers', db.models['account'], {
       role : String
    }, {
        reverse : 'privateMessagesByTag',
        autoFetch : true
    });

    privateMessage.hasMany('taggedGoals', db.models['goal'], {
       role: String
    }, {
        reverse : 'privateMessagesByTag',
        autoFetch : true
    });

    /*privateMessage.hasMany('messages', db.models['message'], {
       LatestMessageDate : Date,
       role : String,
       order : Number
    }, {
       reverse: 'messages',
       autoFetch: true
    });*/

    return cb();
}

export = definePrivateMessageModel;
