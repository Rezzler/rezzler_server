/// <reference path="../references.ts" />

import orm = require('orm');

function defineAccountModel(db : orm.ORM, cb : Function) {
    var Account = db.define('account', {
        firstName : String,
        lastName : { type: 'text'},
        createdDate : { type: 'date', time: true },
        birthDate : Date,
        emailAddress : String,
        passwordHash : String,
        passwordSalt : String,
        // Gender could be needed??? all girls club?
        gender : String,
        // Nickname in case they join a group anonymously.
        Nickname : String
    });

    /*Account.hasMany('friends', db.models['account'], {
        isAccountabilityPartner: Boolean
    }, {
            reverse: 'friend',
            autoFetch: true
        });
    
    Account.hasMany('family', db.models['account'], {
        relation: String
    }, {
            //reverse: 'family2',//the reverse can't be the same? do he have to have a reverse?
            //I guess I'm just a little fuzzy as to what I need this for... 
            //I mean, I think it's for reverse lookup? yes? and if so... I would want it in the others "family" list.
            //Will have have to have some type of bridge table?
            autoFetch: true
        });*/

    return cb();
}

export = defineAccountModel;