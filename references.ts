/// <reference path="ts_modules/node.d.ts" />
/// <reference path="ts_modules/express.d.ts" />
/// <reference path="ts_modules/orm.d.ts"/>
/// <reference path="ts_modules/morgan.d.ts" />
/// <reference path="ts_modules/body-parser.d.ts" />
/// <reference path="ts_modules/passport.d.ts" />
/// <reference path="ts_modules/passport-strategy.d.ts" />
/// <reference path="ts_modules/passport-local.d.ts" />
/// <reference path="ts_modules/rezzlerExpressExtensions.ts" />
/// <reference path="ts_modules/stylus.d.ts" />
/// <reference path="ts_modules/moment.d.ts" />

/// <reference path="classes/common/Common.ts" />

/// <reference path="classes/accounts/AccountCommands.ts" />
/// <reference path="classes/accounts/IAccountManager.ts" />

/// <reference path="classes/groups/GroupCommands.ts" />
/// <reference path="classes/groups/IGroupManager.ts" />

/// <reference path="classes/goals/GoalEnums.ts" />
/// <reference path="classes/goals/GoalCommands.ts" />
/// <reference path="classes/goals/IGoalManager.ts" />

/// <reference path="classes/sessions/SessionCommands.ts" />
/// <reference path="classes/sessions/ISessionManager.ts" />
/// <reference path="classes/sessions/SessionEnums.ts" />



