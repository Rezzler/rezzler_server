/// <reference path="references.ts" />

import express = require( 'express' );
import DB = require( './db/db' );
import orm = require( "orm" );
import log = require( "morgan" );
import bodyParser = require( 'body-parser' );
import passport = require( 'passport' );
var cookieParser = require( 'cookie-parser' );
var session = require( 'cookie-session' );
import LocalStrategy = require( 'passport-local' );

import AccountManager = require( './classes/accounts/AccountManager' );
import GroupManager = require( './classes/groups/GroupManager' );
import GoalManager = require( './classes/goals/GoalManager' );
import SessionManager = require( './classes/sessions/SessionManager' );

var app = express();

//---------------------------
// Standard middleware
//---------------------------
app.set( 'views', __dirname + '/views' )
app.set( 'view engine', 'jade' )
app.use( log( 'combined' ) );
app.use( bodyParser.urlencoded( { extended: true }) );
app.use( bodyParser.json() );
app.use( cookieParser() );
app.use( session( { keys: ['secretkey1', 'secretkey2', '...'] }) );
// Configure passport middleware
app.use( passport.initialize() );
app.use( passport.session() );

//---------------------------
// Database
//---------------------------
var db: DB = new DB();
db.connect( function ( err ) {
    if ( err )
        throw err;

    db.sync( function ( err ) {
        if ( err ) {
            throw err;
        }
    });
});

var am: AccountManager.AccountManager = new AccountManager.AccountManager( db );
var gm: GroupManager = new GroupManager( db );
var goal: GoalManager = new GoalManager( db );
var sm: SessionManager = new SessionManager( db );
app.use( function ( req: express.Request, res, next ) {
    req.AccountManager = am;
    req.GroupManager = gm;
    req.GoalManager = goal;
    req.SessionManager = sm;
    next();
});

//---------------------------
// API middleware
//---------------------------
import route_accounts = require( './routes/accounts' );
import route_groups = require( './routes/groups' );
import route_goals = require( './routes/goals' );
import route_test = require( './routes/test' );
import route_sessions = require('./routes/sessions');
app.use( '/api/accounts', route_accounts );
app.use( '/api/groups', route_groups );
app.use( '/api/goals', route_goals );
app.use( '/api', route_test );
app.use('/api/sessions', route_sessions);

//---------------------------
// Authentication
//---------------------------
passport.use( new LocalStrategy.Strategy(
    function ( username, password, done ) {
        var loginCommand: AccountCommands.LoginCommand = {
            Email: username,
            Password: password,
        };
        console.log( "Attempting login" );
        am.login( loginCommand, function ( err, user ) {
            if ( err ) {
                console.log( "error logging in" );
                return done( err );
            }
            if ( !user ) {
                return done( null, false, { message: 'Incorrect username.' });
            }
            return done( null, user );
        });
    }) );

passport.serializeUser( function ( user, done ) {
    // Save the account ID in the cookie
    done( null, user.id );
});

passport.deserializeUser( function ( id, done ) {
    // The value read out of the cookie is the account ID
    // load from the DB
    console.log( "deserializing using id of " + id );
    /*db.AccountDB.find({ id: id}, function ( err, users ) {
        if(users && users.length > 0)
            return done( err, users );
        else
            return done(err, null);
    });*/
    db.AccountDB.get(id, (err, user) => {
       return done(err, user);
    });
});

app.post( '/login',
    function ( req, res, next ) {
        console.log( "got here" );
        console.log( req.body );
        next();
    },
    passport.authenticate( 'local' ),
    function ( req, res ) {
        res.redirect( '/' );
    }
    );


//---------------------------
// Web html middleware
//---------------------------
import web_accounts = require( './routes/web_accounts' );
app.use( '/', web_accounts );
app.use( express.static( __dirname + '/public' ) );

app.use( function ( err: Error, req, res, next ) {
    console.log( "GOT ERROR HERE" );
    res.status(/*err.status ||*/ 500 );
    res.send( {
        message: err.message,
        //error: err
    });
});

var port: Number = process.env.PORT || 8080;
app.listen( port, function () {
    console.log( "Demo Express server listening on port %d in %s mode", port, app.settings.env );
});
