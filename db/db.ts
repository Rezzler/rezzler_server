
import orm = require('orm');

import AccountsCommon = require('../classes/accounts/AccountsCommon');
import GroupCommon = require('../classes/groups/GroupCommon');
import GoalCommon = require('../classes/goals/GoalCommon');
import SessionCommon = require('../classes/sessions/SessionCommon');
import PostCommon = require('../classes/encouragementPosts/PostCommon');

var connStr = "pg://master:recountable@recountable.cthats9unxrt.us-east-1.rds.amazonaws.com/recountable?pool=true";
var modelDir = "../models/index";



// The model definitions
// NOTE - This is just a hack that makes typescript
//        compile all of our files because they are referenced.
import indexLoader               = require('../models/index');
import accountModel              = require('../models/account');
import accountSettingsModel      = require('../models/accountSettings');
import goalModel                 = require('../models/goal');
import goalSettings              = require('../models/goalSettings');
import groupModel                = require('../models/group');
import notificationSettingsModel = require('../models/notificationSettings');
import messageModel              = require('../models/message');
import privateMessageModel       = require('../models/privateMessage');
import sessionModel              = require('../models/session');

class DB {
    private static _db : orm.ORM; // the ORM db
    
    public AccountDB : orm.Model<AccountsCommon.IAccount, AccountsCommon.IAccountInstance>;
    public GroupDB : orm.Model<GroupCommon.IGroupBase, GroupCommon.IGroupInstance>;
    public GoalDB : orm.Model<GoalCommon.IGoalBase, GoalCommon.IGoalInstance>;
    public SessionDB: orm.Model<SessionCommon.ISession, SessionCommon.ISessionInstance>;
    public EncouragementPostDB: orm.Model<PostCommon.IPostBase, PostCommon.IPostInstance>;
    
    constructor() {
        
    }
    
    public connect(cb : Function) {
        console.log("connecting to database");
        orm.connect(connStr, (err, db) => {
            if(err)
                throw err;
            
            DB._db = db;
                
            console.log("successfully connected.");
            
            var dbsettings : any = db.settings;
            dbsettings.set('instance.cache', false);
            
            console.log("defining models");
            
            db.load(modelDir, (err) => {
                if(err) {
                    console.error("Error defining models");
                    console.error(err);
                    throw err;
                }
                    
                console.info("models defined");
                this.AccountDB = <orm.Model<AccountsCommon.IAccount, AccountsCommon.IAccountInstance>>db.models['account'];
                this.GroupDB   = <orm.Model<GroupCommon.IGroupBase,  GroupCommon.IGroupInstance>>db.models['group'];
                this.GoalDB    = <orm.Model<GoalCommon.IGoalBase,    GoalCommon.IGoalInstance>>db.models['goal'];
                this.SessionDB = <orm.Model<SessionCommon.ISession, SessionCommon.ISessionInstance>>db.models['session'];
                this.EncouragementPostDB = <orm.Model<PostCommon.IPostBase, PostCommon.IPostInstance>>db.models['encouragementPosts'];
                
                cb();
            });
        });
    }
    
    public sync(cb : Function) {
        console.info("syncing model");
        
        DB._db.sync(function(err) {
            if(err) {
                console.error("schema failed to sync");
                throw err;
            }
             
            console.info("schema synced");
            cb();
        });
    }
}

export = DB;



