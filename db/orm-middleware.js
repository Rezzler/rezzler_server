
var orm = require('orm');

var connStr = "pg://master:recountable@recountable.cthats9unxrt.us-east-1.rds.amazonaws.com/recountable?pool=true";
var modelDir = "../models/index";

orm.settings.set("instance.cache", false);

module.exports = orm.express(connStr, {
    define: function (db, models, next) {
        console.log("loading database");

        db.load(modelDir, function (err) {
            // loaded!
            if(err)
            {
                console.error("db failed to load");
                console.error(err);
                console.error(err.stack);
                process.exit(1);
            }
            else {
                console.info("db loaded");
                models.account = db.models.account;
                models.group = db.models.group;
                models.goal = db.models.goal;
                
                console.info("syncing model");
                db.sync(function(err) {
                    if(err) {
                        console.error("schema failed to sync");
                        console.error(err);
                    }
                    else
                    {
                        console.info("schema synced");
                    }
                });
            }
        });
        
        next();
    },
    
    error: function(err) {
        console.error("database connection failed");
        console.error(err);
    }
});