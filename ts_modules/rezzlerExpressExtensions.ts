/// <reference path="./express.d.ts" />

module Express {
    export interface Request {
        AccountManager: IAccountManager;
        GroupManager: IGroupManager;
        GoalManager: IGoalManager;
        SessionManager: ISessionManager;
    }
}

