rm -rf builds && \
tsc --module commonjs app.ts --outDir builds && \
cp -Rf node_modules builds && \
cp -Rf views builds && \
cp -Rf public builds && \
cd builds && \
zip -rq ../../rezzler.zip *
