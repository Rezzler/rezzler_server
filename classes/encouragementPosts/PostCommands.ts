﻿/// <reference path="../../references.ts" />

module PostCommands {

    export interface PostEncoragementCommand {
        accountId: Number;
        message: String;
    }

    export interface GetEncoragementPostsCommand {

    }

    export interface getGoalEncoragementPostsCommand {
        goalId: number;
    }


};