﻿/// <reference path="../../references.ts" />

interface IPostManager {

    getAllEncoragementPosts(command: PostCommands.GetEncoragementPostsCommand, user: any, cb: Function);
    getGoalEncoragementPosts(command: PostCommands.getGoalEncoragementPostsCommand, user: any, cb: Function);
    postEncoragement(command: PostCommands.PostEncoragementCommand, user: any, cb: Function);

}