﻿/// <referance path="../../reference.ts" />

import DB = require("../../db/db");
import ORM = require("orm");
import Common = require("../common/Common");
import AccountsCommon = require('../accounts/AccountsCommon');
import PostCommon = require("./PostCommon");

class PostManager implements IPostManager {
    private _db: DB;

    constructor(db: DB) {
        this._db = db;
    }

    public getAllEncoragementPosts(command: PostCommands.GetEncoragementPostsCommand, user: AccountsCommon.IAccountInstance, cb: Function) {
        this._db.AccountDB.get(user,(err, encouragementPosts) => {
            if (err) {
                //error stuff..
            }
            return cb(null, encouragementPosts);
        });
    }

    public getGoalEncoragementPosts(command: PostCommands.getGoalEncoragementPostsCommand, user: AccountsCommon.IAccountInstance, cb: Function) {

    }

    public postEncoragement(command: PostCommands.PostEncoragementCommand, user: AccountsCommon.IAccountInstance, cb: Function) {

    }


}