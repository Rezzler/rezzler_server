﻿/// <reference path="../../references.ts" />

import orm = require('orm');
import AccountsCommon = require('../accounts/AccountsCommon');

module PostCommon {

    export class IPostBase {
        id: number;
        from: AccountsCommon.IAccountInstance;
        message: string;
    };

    export interface IPostInstance extends IPostBase, orm.Instance { }

}


export = PostCommon;