/// <reference path="../../references.ts" />

import DB = require( "../../db/db" );
import ORM = require( "orm" );

import Common = require( "../common/Common" );
import AccountsCommon = require( "../accounts/AccountsCommon" );
import GroupCommon = require( "./GroupCommon" );

class GroupManager implements IGroupManager {
    private _db: DB;

    constructor( db: DB ) {
        this._db = db;
    }

    public listAllGroups( cb: Function ) {
        this._db.GroupDB.find( {}, ( err, groups ) => {
            if ( err )
                return cb(err);
            else
                return cb(null, groups);
        });
    }

    public getGroup( command: GroupCommands.GroupQuery, cb: Function ) {
        this._db.GroupDB.get( command.GroupID, ( err, group ) => {
            if ( err ) {
                cb( new Common.Result<any>( false, "error", 1, err ) );
            }
            else {
                cb( new Common.Result<any>( true, "", 0, group ) );
            }
        });
    }

    public searchGroups( command: GroupCommands.GroupQuery, cb: Function ) {
        var params: { [property: string]: any } = {};

        if ( command.GroupID != null ) {
            params["id"] = command.GroupID;
        }

        if ( command.Name != null ) {
            params["name"] = ORM.like( "%" + command.Name + "%" );
        }

        if ( command.Latitude != null && command.Longitude != null ) {
            params["latitude"] = ORM.between( command.Latitude - 1, command.Latitude + 1 );
            params["longitude"] = ORM.between( command.Longitude - 1, command.Longitude + 1 );
        }

        this._db.GroupDB.find( params, ( err: Error, groups ) => {
            if ( err ) {
                return cb( err );
            }
            else {
                return cb( null, groups );
            }
        });
    }

    public createGroup( command: GroupCommands.CreateGroupCommand, cb: Function ) {
        var newGroup: GroupCommon.IGroupBase = {
            name: command.Name,
            latitude: command.Latitude,
            longitude: command.Longitude,
            createdDate: new Date(),
            isPublic: command.isPublic,
            members: null
        };

        this._db.GroupDB.create( [newGroup], ( err: Error, items: GroupCommon.IGroupBase[] ) => {
            if ( err ) {
                return cb( err );
            }

            return cb( null, items[0] );
        });
    }

    public joinGroup( command: GroupCommands.JoinGroupCommand, cb: Function ) {
        this._db.GroupDB.get( command.GroupID, ( err, group ) => {
            if ( err ) {
                return cb( new Common.Result( false, "get group err", 1, err ) );
            }

            this._db.AccountDB.get( command.AccountID, ( err, account ) => {
                if ( err ) {
                    return cb( new Common.Result( false, "get account err", 1, err ) );
                }

                group.hasMembers( [account], ( err, exists ) => {
                    if ( exists ) {
                        return cb( new Common.Result( false, "account already in group" ) );
                    }

                    group.addMembers( [account], { joinDate: new Date(), role: command.Role }, ( err ) => {
                        if ( err ) {
                            return cb( new Common.Result( false, "add member error", 1, err ) );
                        }

                        group.save( ( err ) => {
                            if ( err ) {
                                return cb( new Common.Result( false, "save error", 1, err ) );
                            }

                            return cb( new Common.Result( true ) );
                        });
                    });
                });
            });
        });
    }

    public leaveGroup( command: GroupCommands.LeaveGroupCommand, cb: Function ) {
        this._db.GroupDB.get( command.GroupID, ( err, group ) => {
            if ( err ) {
                return cb( new Common.Result( false, err ) );
            }

            this._db.AccountDB.get( command.AccountID, ( err, account ) => {
                if ( err ) {
                    return cb( new Common.Result( false, err ) );
                }

                group.hasMembers( [account], ( err, exists ) => {
                    if ( !exists ) {
                        return cb( new Common.Result( false, "not in group" ) );
                    }

                    group.removeMembers( [account], {}, ( err ) => {
                        if ( err ) {
                            return cb( new Common.Result( false, err ) );
                        }

                        group.save( function ( err ) {
                            if ( err ) {
                                return cb( new Common.Result( false, err ) );
                            }

                            return cb( new Common.Result( true ) );
                        });
                    });
                });
            });
        });
    }
};

export = GroupManager;