/// <reference path="../../references.ts" />
import orm = require('orm');
import AccountsCommon = require("../accounts/AccountsCommon");

module GroupCommon
{
    export interface IGroupBase
    {
         name : string;
         createdDate: Date;
         isPublic : boolean;
         latitude : number;
         longitude : number;
         
         members : AccountsCommon.IAccount[];
    }

    export interface IGroupInstance extends IGroupBase, orm.Instance {
        
    }
}

export = GroupCommon;
