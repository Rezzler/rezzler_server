/// <reference path="../../references.ts" />

//import Common = require( "../common/Common" );

module GroupCommands {

    export interface CreateGroupCommand {
        GroupID: number;
        OwnerAccountID: number;
        Name: string;
        Latitude: number;
        Longitude: number;
        isPublic: boolean;
    }

    export interface GroupQuery {
        GroupID?: number;
        Name?: string;
        Latitude?: number;
        Longitude?: number;
    }

    export interface JoinGroupCommand {
        AccountID: Number;
        GroupID: Number;
        Role: String;
    }

    export interface LeaveGroupCommand {
        AccountID: Number;
        GroupID: Number;
    }

}

//export = GroupCommands;