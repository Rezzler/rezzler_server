/// <reference path="../../references.ts" />

interface IGroupManager {
    listAllGroups( cb: Function );
    getGroup( command: GroupCommands.GroupQuery, cb: Function );
    searchGroups( command: GroupCommands.GroupQuery, cb: Function );
    createGroup( command: GroupCommands.CreateGroupCommand, cb: Function );
    joinGroup( command: GroupCommands.JoinGroupCommand, cb: Function );
    leaveGroup( command: GroupCommands.LeaveGroupCommand, cb: Function );
}

