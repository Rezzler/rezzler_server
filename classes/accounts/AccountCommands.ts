/// <reference path="../../references.ts" />
//import Common = require('../common/Common');

module AccountCommands {

    export interface AccountQuery /*extends Common.BaseCommand*/ {
        AccountID: Number;
    }

    export interface CreateUserCommand /*extends Common.BaseCommand*/ {
        FirstName: String;
        LastName: String;
        BirthDate: Date;
        EmailAddress: String;
        Password: String;
        Gender: String;
        Nickname: String;
    }

    export interface LoginCommand /*extends Common.BaseCommand*/ {
        Email: String;
        Password: String;
    }

    export interface FindUserCommand {
        SearchString: String;
    }

}

//export = AccountCommands;
