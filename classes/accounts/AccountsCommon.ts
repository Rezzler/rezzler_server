/// <reference path="../../references.ts" />

import orm = require('orm');

import SessionCommon = require('../sessions/SessionCommon');

module AccountsCommon
{
    export interface IAccount {
        firstName: string;
        lastName: string;
        createdDate: Date;
        birthDate: Date;
        emailAddress: string;
        passwordHash: string;
        passwordSalt: string;
        Nickname: string;
        
        sessions: SessionCommon.ISessionInstance[];
        friends: IAccount[];
        family: IAccount[];
    };
    
    export interface IAccountInstance extends IAccount, orm.Instance {
        _singleton_uid : string;
        addGoals(goals : any[], cb : Function) : any;
    }
}

export = AccountsCommon;
