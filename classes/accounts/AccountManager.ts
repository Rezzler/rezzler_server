/// <reference path="../../references.ts" />
import orm = require( 'orm' );
import Common = require( "../common/Common" );
import AccountsCommon = require( './AccountsCommon' );
import Hash = require( "../common/Hash" );
import DB = require( "../../db/db" );

module AccountManager {
    export class AccountManager implements IAccountManager {
        private _db: DB;

        constructor( db: DB ) {
            this._db = db;
        }

        public findAccountsMatchingString(command: AccountCommands.FindUserCommand, cb: Function) {
            if (command.SearchString) {
                this._db.AccountDB.find({ firstName: command.SearchString }, function (err, accounts: AccountsCommon.IAccountInstance[]) {
                    if (err) {
                        return cb(err, null);
                    }
                    else {
                        return cb(null, accounts);
                    }
                });
            }
        }

        public login( command: AccountCommands.LoginCommand, cb: ( err: Error, account: any ) => void ) {
            if ( command.Email ) {
                if ( command.Password ) {
                    this._db.AccountDB.find( { emailAddress: command.Email }, function ( err, accounts: AccountsCommon.IAccountInstance[] ) {

                        if ( err ) {
                            console.log("got error from find: " + err);
                            return cb( err, null );
                        }
                        else {
                            if ( accounts.length > 0 ) {
                                // Hash input
                                var hashResult = Hash.hashPasswordWithSalt( command.Password, accounts[0].passwordSalt );

                                if ( hashResult == accounts[0].passwordHash ) {
                                    cb( null, accounts[0] );
                                }
                                else {
                                    cb( new Error( "wrong password" ), null );
                                }
                            }
                            else {
                                cb(null, null);
                            }
                        }
                    });
                }
                else {
                    cb( new Error("missing password" ), null );
                }
            }
            else {
                cb( new Error("email missing" ) , null);
            }
        }

        public listAllAccounts( cb: Function ) {
            this._db.AccountDB.find( {}, function ( err, accounts ) {
                if ( err )
                    return cb( new Common.Result<any>( false, "error", 1, err ) );
                else
                    return cb( new Common.Result<any>( true, "", 0, accounts ) );
            });
        }

        public getAccount( query: AccountCommands.AccountQuery, cb: Function ) {
            this._db.AccountDB.get( query.AccountID, function ( err, account ) {
                if ( err ) {
                    cb( new Common.Result<any>( false, "error", 1, err ) );
                }
                else {
                    cb( new Common.Result<any>( true, "", 0, account ) );
                }
            });
        }

        public createUser( command: AccountCommands.CreateUserCommand, cb: Function ) {

            var hashResult: Hash.HashResult = Hash.createHashAndSalt( command.Password );

            this._db.AccountDB.create( [
                {
                    firstName: command.FirstName,
                    createdDate: new Date(),
                    NickName: command.Nickname,
                    gender: command.Gender,
                    emailAddress: command.EmailAddress,
                    passwordHash: hashResult.PasswordHash,
                    passwordSalt: hashResult.PasswordSalt
                }], ( err, items ) => {
                    if ( err ) {
                        return cb( new Common.Result( false, err ) );
                    }
                    else {
                        return cb( new Common.Result( true, "", 0, items ) );
                    }
                });
        }
    }
}

export = AccountManager;