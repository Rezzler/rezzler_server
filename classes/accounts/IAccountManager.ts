/// <reference path="../../references.ts" />

interface IAccountManager
{
    login(command : AccountCommands.LoginCommand, cb : (err : Error, account : any) => void);
    listAllAccounts(cb : Function);
    getAccount(query : AccountCommands.AccountQuery, cb : Function );
    createUser(command: AccountCommands.CreateUserCommand, cb: Function);
    findAccountsMatchingString(command: AccountCommands.FindUserCommand, cb: Function);
}