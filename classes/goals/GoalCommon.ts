/// <reference path="../../references.ts" />

import orm = require( 'orm' );
import SessionCommon = require("../sessions/SessionCommon")

module GoalCommon {

    export class IGoalBase {
        id: number;
        type: GoalEnums.GoalType;
        startDate: Date;
        freqUnit: GoalEnums.GoalFrequency;
        freqCount: number;
        isActive: boolean;
        owner: any;
        
        progressOverall: IGoalProgressSnapshot;
        progressCurrent: IGoalProgressSnapshot;
        progressHistory: IGoalProgressSnapshot[];
        
    };

    export interface IGoalInstance extends IGoalBase, orm.Instance {    }

    export interface IGoalProgressSnapshot {
        startDate : Date;
        endDate :Date;
        percentComplete: Number;
        numCompleted : Number;
        numTotal : Number;
    }

    export interface IGoalProgressCurrent extends IGoalProgressSnapshot {
        sessions : SessionCommon.ISession[]  
    }
}

export = GoalCommon;
