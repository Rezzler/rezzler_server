/// <reference path="../../references.ts" />
import moment = require( 'moment' );
import GoalCommon = require( './GoalCommon' );
import SessionCommon = require( '../sessions/SessionCommon' );

interface IDateRange {
    start: moment.Moment;
    end: moment.Moment;
}


class GoalProgressMagic {

    public ComputeGoalProgressCurrent( goal: GoalCommon.IGoalBase, sessions: SessionCommon.ISessionInstance[], cb: Function ) {
        var total = 0;
        var current = 0;
        var progress: GoalCommon.IGoalProgressSnapshot;

        if ( goal.type == GoalEnums.GoalType.CheckIn ) {
            progress = this.computeGoalProgressCheckin( goal, sessions );
        } else if ( goal.type == GoalEnums.GoalType.Steps ) {
            progress = this.computeGoalProgressSteps( goal, sessions );
        } else if (goal.type == GoalEnums.GoalType.FootDistance) {
            progress = this.computeGoalProgressFootDistance(goal, sessions);
        } else if (goal.type == GoalEnums.GoalType.BikingDistance) {
            progress = this.computeGoalProgressBikingDistance(goal, sessions);
        }

        if ( progress )
            return cb( null, progress );
        else
            return cb( new Error(), null );
    }

    private computeGoalProgressSteps( goal: GoalCommon.IGoalBase, sessions: SessionCommon.ISessionInstance[] )
        : GoalCommon.IGoalProgressSnapshot {
        var total: number = goal.freqCount;
        var current: number = 0;
        var goalWindow: IDateRange = this.computeGoalCurrentWindow( goal );
        var matchingSessions : SessionCommon.ISession[] = [];

        if ( sessions ) {
            sessions.forEach( ( session ) => {
                if ( session.sessionType == SessionEnums.SessionType.WalkingRunning ) {
                    var sessionStart = moment( session.createdDate );
                    if ( sessionStart.isAfter( goalWindow.start ) && sessionStart.isBefore( goalWindow.end ) ) {
                        current += session.totalSteps;
                        matchingSessions.push(session)
                    }
                }
            });
        }

        var percent: Number = 0;
        if ( total > 0 ) {
            percent = current / total * 100;
        }

        return <GoalCommon.IGoalProgressCurrent> {
            startDate: goalWindow.start.toDate(),
            endDate: goalWindow.end.toDate(),
            percentComplete: percent,
            numCompleted: current,
            numTotal: total,
            sessions: matchingSessions
        };
    }

    private computeGoalProgressFootDistance( goal: GoalCommon.IGoalBase, sessions: SessionCommon.ISessionInstance[] )
        : GoalCommon.IGoalProgressSnapshot {
        var total: number = goal.freqCount;
        var current: number = 0;
        var goalWindow: IDateRange = this.computeGoalCurrentWindow( goal );
        var matchingSessions : SessionCommon.ISession[] = [];

        if ( sessions ) {
            sessions.forEach( ( session ) => {
                if ( session.sessionType == SessionEnums.SessionType.WalkingRunning ) {
                    var sessionStart = moment( session.createdDate );
                    if ( sessionStart.isAfter( goalWindow.start ) && sessionStart.isBefore( goalWindow.end ) ) {
                        current += session.totalDistance;
                        matchingSessions.push(session)
                    }
                }
            });
        }

        var percent: Number = 0;
        if ( total > 0 ) {
            percent = current / total * 100;
        }

        return <GoalCommon.IGoalProgressCurrent> {
            startDate: goalWindow.start.toDate(),
            endDate: goalWindow.end.toDate(),
            percentComplete: percent,
            numCompleted: current,
            numTotal: total,
            sessions: matchingSessions
        };
    }

    private computeGoalProgressBikingDistance( goal: GoalCommon.IGoalBase, sessions: SessionCommon.ISessionInstance[] )
        : GoalCommon.IGoalProgressSnapshot {
        var total: number = goal.freqCount;
        var current: number = 0;
        var goalWindow: IDateRange = this.computeGoalCurrentWindow( goal );
        var matchingSessions : SessionCommon.ISession[] = [];

        if ( sessions ) {
            sessions.forEach( ( session ) => {
                if ( session.sessionType == SessionEnums.SessionType.Biking ) {
                    var sessionStart = moment( session.createdDate );
                    if ( sessionStart.isAfter( goalWindow.start ) && sessionStart.isBefore( goalWindow.end ) ) {
                        current += session.totalDistance;
                        matchingSessions.push(session)
                    }
                }
            });
        }

        var percent: Number = 0;
        if ( total > 0 ) {
            percent = current / total * 100;
        }

        return <GoalCommon.IGoalProgressCurrent> {
            startDate: goalWindow.start.toDate(),
            endDate: goalWindow.end.toDate(),
            percentComplete: percent,
            numCompleted: current,
            numTotal: total,
            sessions: matchingSessions
        };
    }

    private computeGoalProgressCheckin( goal: GoalCommon.IGoalBase, sessions: SessionCommon.ISessionInstance[] ): GoalCommon.IGoalProgressSnapshot {
        var total = goal.freqCount;
        var current = 0;
        var goalWindow: IDateRange = this.computeGoalCurrentWindow( goal );
        var matchingSessions : SessionCommon.ISession[] = [];

        if ( sessions ) {
            sessions.forEach( ( session ) => {
                if ( session.sessionType == SessionEnums.SessionType.CheckIn ) {
                    var sessionStart = moment( session.createdDate );
                    console.log( "consider checkin: window start: " + goalWindow.start + " end: " + goalWindow.end + " checkin: " + sessionStart );
                    if ( sessionStart.isAfter( goalWindow.start ) && sessionStart.isBefore( goalWindow.end ) ) {
                        // TODO? - check GPS coords and only count checkins specific to this goal
                        current += 1;
                        matchingSessions.push(session)
                    }
                }
            });
        }

        var percent = 0;
        if ( total > 0 ) {
            percent = current / total * 100;
        }

        return <GoalCommon.IGoalProgressCurrent> {
            startDate: goalWindow.start.toDate(),
            endDate: goalWindow.end.toDate(),
            percentComplete: percent,
            numCompleted: current,
            numTotal: total,
            sessions: matchingSessions
        };

    }

    private computeGoalCurrentWindow( goal: GoalCommon.IGoalBase ): IDateRange {
        switch ( goal.freqUnit ) {
            case GoalEnums.GoalFrequency.Daily:
                break;

            case GoalEnums.GoalFrequency.Weekly:
                // This is really ugly, what about using day of week to calculate?

                var now: moment.Moment = moment();

                // Get initial the starting date (by deleting time information)
                var start: moment.Moment = moment( goal.startDate );
                start.hours( 0 );
                start.minutes( 0 );
                start.seconds( 0 );
                start.milliseconds( 0 );

                // Get initial ending date (1 week after the start)
                var end: moment.Moment = start.clone().add( 7, 'days' );

                // Move forward 1 week at a time until we get the window
                // that contains "now"
                while ( end.isBefore( now ) ) {
                    start = end.clone();
                    end = start.clone().add( 7, 'days' );
                }
                return <IDateRange>{ start: start, end: end };
                break;

            case GoalEnums.GoalFrequency.Monthly:
                // This is really ugly, what about using day of month to calculate?
                var now: moment.Moment = moment();

                // Get initial the starting date (by deleting time information)
                var start: moment.Moment = moment( goal.startDate );
                start.hours( 0 );
                start.minutes( 0 );
                start.seconds( 0 );
                start.milliseconds( 0 );

                // Get initial ending date (1 month after the start)
                var end: moment.Moment = start.clone().add( 1, 'months' );

                // Move forward 1 month at a time until we get the window
                // that contains "now"
                while ( end.isBefore( now ) ) {
                    start = end.clone();
                    end = start.clone().add( 1, 'months' );
                }
                return <IDateRange>{ start: start, end: end };
                break;

            default:
                console.log( "unknown goal freq. goal: %j", goal );
                break;
        }

        return null;
    }
}


export = GoalProgressMagic;





