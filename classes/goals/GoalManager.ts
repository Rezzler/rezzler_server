/// <reference path="../../references.ts" />

import DB = require( "../../db/db" );
import ORM = require( "orm" );
import Common = require( "../common/Common" );
import AccountsCommon = require( '../accounts/AccountsCommon' );
import GoalCommon = require( './GoalCommon' );
import GoalProgressMagic = require('./GoalProgressMagic');

class GoalManager implements IGoalManager {
    private _db: DB;

    constructor( db: DB ) {
        this._db = db;
    }

    public listAllGoals( cb: Function ) {
        this._db.GoalDB.find( {}, ( err, goals: GoalCommon.IGoalInstance[] ) => {
            if ( err )
                cb( err );
            else
                cb( null, goals );
        });
    }

    public getGoal( command: GoalCommands.GoalQuery, cb: Function ) {
        this._db.GoalDB.get( command.GoalId, ( err, goal ) => {
            if ( err ) {
                return cb(err, null);
            }
            else {
                return cb(null, goal);
            }
        });
    }

    public searchGoals( command: GoalCommands.GoalQuery, cb: Function ) {
        var params: { [property: string]: any } = {};

        if ( command.GoalId != null ) {
            params["id"] = command.GoalId;
        }

        this._db.GoalDB.find( params, ( err: Error, groups ) => {
            if ( err ) {
                return cb( new Common.Result( false, "error", 1, err ) );
            }
            else {
                return cb( new Common.Result( true, "", 0, { groups: groups }) );
            }
        });
    }

    public createGoal( command: GoalCommands.CreateGoalCommand, cb: Function ) {
        var newGoal: GoalCommon.IGoalBase = new GoalCommon.IGoalBase();
        newGoal.type = command.Type;
        newGoal.freqUnit = command.frequencyType;
        newGoal.freqCount = command.frequencyCount;
        newGoal.isActive = true;
        newGoal.startDate = new Date();

        this._db.GoalDB.create( [newGoal], ( err: Error, items: GoalCommon.IGoalBase[] ) => {
            if ( err ) {
                console.log( "create goal failure" );
                return cb( err );
            }
            else {
                console.log( "create goal success" );
                return cb( null, items );
            }
        });
    }

    public createGoalForUser( command: GoalCommands.CreateGoalCommand, user: AccountsCommon.IAccountInstance, cb: Function ) {
        var newGoal: GoalCommon.IGoalBase = new GoalCommon.IGoalBase();
        newGoal.type = command.Type;
        newGoal.freqUnit = command.frequencyType;
        newGoal.freqCount = command.frequencyCount;
        newGoal.isActive = true;
        newGoal.startDate = new Date();

        this._db.GoalDB.create( [newGoal], ( err: Error, items: GoalCommon.IGoalBase[] ) => {
            if ( err ) {
                console.log( "create goal failure" );
                return cb( err );
            }

            user.addGoals( items, ( err2 ) => {
                if ( err2 ) {
                    return cb( err2 );
                }

                // delete user from ORM cache
                ORM.singleton.clear( user._singleton_uid );
                return cb( null, items );
            });
        });
    }

    public deleteGoal( goalID : number, cb: Function ) {
        this._db.GoalDB.get( goalID, ( err, goal: GoalCommon.IGoalInstance ) => {
            if ( err ) {
                return cb( err );
            }

            goal.remove( ( err2 ) => {
                if(err2) {
                    return cb(err2);
                }
                
                return cb(null);
            });
        });
    }

    public startGoal( command: GoalCommands.StartGoalCommand, cb: Function ) {
        this._db.GoalDB.get( command.GoalId, ( err, goal ) => {
            if ( err ) {
                return cb( new Common.Result( false, "get group err", 1, err ) );
            }
            goal.isActive = true;
        });
    }

    public pauseGoal( command: GoalCommands.PauseGoalCommand, cb: Function ) {
        this._db.GoalDB.get( command.GoalId, ( err, goal ) => {
            if ( err ) {
                return cb( new Common.Result( false, err ) );
            }
            goal.IsActive = false;
        });
    }

    public stopGoal( command: GoalCommands.StopGoalCommand, cb: Function ) {
        this._db.GoalDB.get( command.GoalId, ( err, goal ) => {
            if ( err ) {
                return cb( new Common.Result( false, err ) );
            }
            //delete goal? should it ever be deleted?
        });
    }
};

export = GoalManager;