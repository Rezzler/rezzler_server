/// <reference path="../../references.ts" />

module GoalCommands {

    export interface CreateGoalCommand {
        Type: GoalEnums.GoalType;
        frequencyType: GoalEnums.GoalFrequency;
        frequencyCount: number;
    }

    export interface GoalQuery {
        GoalId: Number;
    }

    export interface PauseGoalCommand {
        GoalId: number;
    }

    export interface StartGoalCommand {
        GoalId: number;
    }

    export interface StopGoalCommand {
        GoalId: number;
    }

    export interface UpdateGoalCommand {
        Type: GoalEnums.GoalType;
        frequencyType: GoalEnums.GoalFrequency;
        frequencyCount: number;
        isActive: boolean;
    }
    
    export interface DeleteGoalCommand {
        GoalId: number;
    }
};

