
module GoalEnums {
    export enum GoalType {
        Steps = 1,
        WeightLoss = 2,
        CheckIn = 3,
        FootDistance = 4,
        BikingDistance = 5,
    }

    export enum GoalFrequency {
        Daily = 1,
        Weekly = 2,
        Monthly = 3,
        Yearly = 4,
    }
};