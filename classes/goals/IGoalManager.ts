/// <reference path="../../references.ts" />

interface IGoalManager {

    listAllGoals( cb: Function );
    getGoal( command: GoalCommands.GoalQuery, cb: Function );
    searchGoals( command: GoalCommands.GoalQuery, cb: Function );
    createGoal( command: GoalCommands.CreateGoalCommand, cb: Function );
    createGoalForUser( command: GoalCommands.CreateGoalCommand, user: any, cb: Function );
    startGoal( command: GoalCommands.StartGoalCommand, cb: Function );
    pauseGoal( command: GoalCommands.PauseGoalCommand, cb: Function );
    stopGoal( command: GoalCommands.StopGoalCommand, cb: Function );
    deleteGoal(goalID: number, cb: Function);
}

