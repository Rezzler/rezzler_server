/// <reference path="../../references.ts" />

module SessionCommands {
    export interface ISessionQuery {
        sessionID : Number;  
    };
    
    export interface IStartSessionCommand {
        accountID : number;
        sessionType: SessionEnums.SessionType;
        createdDate? : Date;
        latitude? : number;
        longitude? : number; 
        name: string;
        totalSteps?: number;
        totalDistance?: number;
    };
}
