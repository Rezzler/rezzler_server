
import orm = require( 'orm' );

module SessionCommon {
    export interface IGPSMarker {
        timestamp: Date;
        latitude: Number;
        longitude: Number;
    }

    export interface IGPSMarkerInstance extends IGPSMarker, orm.Instance { }

    export interface ISession {
        sessionType: SessionEnums.SessionType;
        createdDate: Date;
        stoppedDate?: Date;
        name: string;
        processed: boolean;
        latitude? : number;
        longitude?: number;
        gpsMarkers: IGPSMarkerInstance[];
        totalSteps?: number;
        totalDistance?: number;
    }

    export interface ISessionInstance extends ISession, orm.Instance { 
        account_id: number;
    }
}

export = SessionCommon;