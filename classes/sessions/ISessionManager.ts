/// <reference path="../../references.ts" />

interface ISessionManager {
    
    startSession(command : SessionCommands.IStartSessionCommand, cb : Function);
    getSessions(cb : Function);
    getRecentSessionsForAccount(accountID : number, cb: Function);
    getSessionsForAccountSinceDate(accountID: number, since : Date, cb: Function);
    
}