/// <reference path="../../references.ts" />

import orm = require( 'orm' );
import DB = require( "../../db/db" );
import SessionCommon = require( "./SessionCommon" );

class SessionManager implements ISessionManager {
    private _db: DB;

    constructor( db: DB ) {
        this._db = db;
    }
    
    public getSessions(cb: Function) {
        this._db.SessionDB.find({}, (err, sessions) => {
            if(err) {
                return cb(err, null);
            } 
            
            return cb(null, sessions);
        });
    }
    
    public getRecentSessionsForAccount(accountID : number, cb: Function) {
        this._db.SessionDB.find({ account_id: accountID }, 3, ["createdDate", "Z"], (err, sessions) => {
            if(err) {
                return cb(err, null);
            } 
            return cb(null, sessions);
        });
    }
    
    public getSessionsForAccountSinceDate(accountID: number, since : Date, cb: Function) {
        this._db.SessionDB.find( {
            account_id: accountID,
            createdDate: orm.gt(since),
        }, (err, sessions) => {
           if(err) {
               return cb(err, null);
           }
           return cb(null, sessions);
        });
    }

    public getSession( query: SessionCommands.ISessionQuery, cb: Function ) {
        this._db.SessionDB.get( query.sessionID, function ( err, results ) {
            if ( err ) {
                return cb( err );
            }

            return cb( null, results );
        });
    }

    public startSession( command: SessionCommands.IStartSessionCommand, cb: Function ) {
        var newSession: SessionCommon.ISession = {
            account_id: command.accountID,
            sessionType: command.sessionType,
            createdDate: command.createdDate,
            stoppedDate: null,
            processed: false,
            latitude: command.latitude,
            longitude: command.longitude,
            name: command.name,
            totalSteps: command.totalSteps,
            totalDistance: command.totalDistance,
            gpsMarkers: [],
        };


        this._db.SessionDB.create( newSession, ( err, newSession ) => {
            return cb( err, newSession );
        });
    }
}

export = SessionManager;