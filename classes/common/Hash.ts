var crypto = require("crypto");


function randomValueHex (len) {
    return crypto.randomBytes(Math.ceil(len/2))
        .toString('hex') // convert to hexadecimal format
        .slice(0,len);   // return required number of characters
}

export class HashResult
{
    constructor(public PasswordSalt : String, public PasswordHash : String)
    {
    }
}


export function createHashAndSalt(plainTextPassword : String) : HashResult {
        var salt = randomValueHex(32);
        
        var sha256 = crypto.createHash("sha256");
        sha256.update(salt, "utf8");
        sha256.update(plainTextPassword, "utf8");
        var result = sha256.digest("base64"); 
        
        return new HashResult(salt, result);
    }
    
export function hashPasswordWithSalt(plainTextPassword : String, passwordSalt : String) : String {
        var sha256 = crypto.createHash("sha256");
        sha256.update(passwordSalt, "utf8");
        sha256.update(plainTextPassword, "utf8");
        return sha256.digest("base64"); 
    }