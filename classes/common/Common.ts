/// <reference path="../../references.ts" />

module Common {
    export class BaseCommand {
        public RequestTime: Date;
    }

    export function error( code: number, message: string ) {
        return new Result<any>( false, message, code, null );
    }

    export class Result<T> {
        public success: boolean;
        public code: number;
        public message: string;
        public value: T;

        constructor( success = false, message = "", code = 1, value = null ) {
            this.success = success;
            this.code = code;
            this.message = message;
            this.value = value;
        }
    }

}

export = Common;