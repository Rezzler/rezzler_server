/// <references path="../../references.ts" />
import express = require( 'express' );
import passport = require( 'passport' );
import orm = require( 'orm' );
import moment = require( 'moment' );
import AccountsCommon = require( '../classes/accounts/AccountsCommon' );
import GoalCommon = require( '../classes/goals/GoalCommon' );
import GoalProgressMagic = require( '../classes/goals/GoalProgressMagic' );

var router = express();

//--------------------------------------------------------------------------------------------------
// HOME PAGE / DASHBOARD
//--------------------------------------------------------------------------------------------------
router.get( '/', function ( req, res ) {
    var theuser: AccountsCommon.IAccountInstance = <AccountsCommon.IAccountInstance>req.user

    if ( req.user ) {
        var old: moment.Moment = moment( "2000-01-01" );
        req.SessionManager.getSessionsForAccountSinceDate( req.user.id, old.toDate(), ( err, sessions ) => {
            //req.user.sessions = sessions;
            var magic = new GoalProgressMagic();
            
            req.user.goals.forEach((goal) => {
                magic.ComputeGoalProgressCurrent(goal, sessions, (err, progress) => {
                    goal.progressCurrent = progress;
                })
            })
            
            res.render( 'dashboard', {
                user: req.user,
                goals: req.user.goals
            })
        });
    } else {
        res.render( 'index', {
            title: 'Home',
            user: req.user,
        })
    }
});


//--------------------------------------------------------------------------------------------------
// REGISTRATION
//--------------------------------------------------------------------------------------------------
router.get( '/register', function ( req, res ) {
    res.render( 'register', {});
});

router.post( '/register', function ( req, res, next ) {
    console.log( 'registering user' );
    var createUsercommand: AccountCommands.CreateUserCommand = {
        FirstName: null,
        LastName: null,
        BirthDate: null,
        EmailAddress: req.body.username,
        Password: req.body.password,
        Gender: null,
        Nickname: null,
    }
    req.AccountManager.createUser( createUsercommand, function ( result ) {
        if ( !result.success ) { console.log( 'error while user register!', result ); return next( result ); }

        console.log( 'user registered!' );

        res.redirect( '/' );
    });
});

//--------------------------------------------------------------------------------------------------
// GROUPS
//--------------------------------------------------------------------------------------------------
router.get( '/groups', function ( req: express.Request, res: express.Response ) {
    if ( req.query.lat || req.query.lng || req.query.name ) {
        var groupQuery: GroupCommands.GroupQuery = {
            Name: req.query.name,
            Latitude: req.query.lat,
            Longitude: req.query.lng,
        }
        req.GroupManager.searchGroups( groupQuery, ( err, data ) => {
            console.log( "returning search results" );
            console.log( data );
            res.render( 'groups', {
                results: data,
                nameValue: req.query.name,
                user: req.user,
            });
        })
    }
    else {
        req.GroupManager.listAllGroups( ( err, data ) => {
            if ( err )
                return res.send( err );

            res.render( 'groups', {
                user: req.user,
                results: data
            });
        });
    }
});


router.get( '/createGroup', function ( req, res ) {
    res.render( 'createGroupForm', { user: req.user });
});


router.post( '/createGroup', function ( req, res ) {
    console.log( 'creating a group' );
    var creatGroupCommand: GroupCommands.CreateGroupCommand = {
        GroupID: null,
        OwnerAccountID: null,
        Name: req.body.name,
        Latitude: req.body.laditude,
        Longitude: req.body.longitude,
        isPublic: false,
    }
    req.GroupManager.createGroup( creatGroupCommand, ( err, newGroup ) => {
        if ( err ) {
            console.log( 'error creating group!' );
            res.send( err );
        }

        console.log( "Group Created" );
        res.redirect( '/createGroup' );
    });
});


//--------------------------------------------------------------------------------------------------
// LOGIN / LOGOUT
//--------------------------------------------------------------------------------------------------
router.post( '/login', passport.authenticate( 'local' ), function ( req, res ) {
    res.redirect( '/' );
});

router.get( '/logout', function ( req, res ) {
    req.logout();
    res.redirect( '/' );
});

//--------------------------------------------------------------------------------------------------
// ACCOUNT DEBUG
//--------------------------------------------------------------------------------------------------
router.get( '/accounts', function ( req: express.Request, res ) {
    req.AccountManager.listAllAccounts( function ( result ) {
        console.log( result );
        res.render( 'accounts', { accounts: result.value });
    });
});



//--------------------------------------------------------------------------------------------------
// GOALS
//--------------------------------------------------------------------------------------------------
router.get( '/addGoal', function ( req: express.Request, res: express.Response ) {
    res.render( 'addGoal', {
        user: req.user,
    })
})

router.get( '/goal/:goalID', function ( req: express.Request, res: express.Response ) {
    var query = <GoalCommands.GoalQuery>{
        GoalId: req.params.goalID
    }
    req.GoalManager.getGoal( query, ( err, goal: GoalCommon.IGoalBase ) => {
        if ( err ) {
            return res.send( err );
        }

        var old: moment.Moment = moment( "2000-01-01" );
        req.SessionManager.getSessionsForAccountSinceDate( req.user.id, old.toDate(), ( err, sessions ) => {

            var magic = new GoalProgressMagic();

            magic.ComputeGoalProgressCurrent( goal, sessions, ( err, progress ) => {
                if ( err ) {
                    return res.render( 'goal', {
                        user: req.user,
                        goal: goal
                    });
                }

                goal.progressCurrent = progress;

                console.log( "Goal Progress: %j", progress );
                return res.render( 'goal', {
                    user: req.user,
                    goal: goal
                });
            });
        });
    })
})

router.post( '/addGoal', function ( req: express.Request, res: express.Response ) {
    if ( req.user ) {
        console.log( "creating goal: " + req.body.goalType );
        if ( req.body.goalType && req.body.goalFreq && req.body.goalNumber ) {
            var command = <GoalCommands.CreateGoalCommand> {
                Type: req.body.goalType,
                frequencyType: req.body.goalFreq,
                frequencyCount: req.body.goalNumber
            }
            req.GoalManager.createGoalForUser( command, req.user, ( err, newGoals ) => {
                if ( err ) {
                    return res.send( err );
                }

                res.redirect( '/' );
            });
        } else {
            res.send( req.body );
        }
    } else {
        // TODO - return forbidden
        return null;
    }
})

router.post( '/deleteGoal', function ( req: express.Request, res: express.Response ) {
    if ( req.user ) {
        var command = <GoalCommands.DeleteGoalCommand> req.body;
        console.log( 'deleting goal: ' + command.GoalId );
        req.GoalManager.deleteGoal( command.GoalId, ( err ) => {
            if ( err ) {
                return res.send( err );
            }

            res.redirect( '/' );
        });
    }
});

//--------------------------------------------------------------------------------------------------
// SESSIONS
//--------------------------------------------------------------------------------------------------
router.get( '/checkin', function ( req: express.Request, res: express.Response ) {
    if ( req.user ) {
        res.render( 'checkin', {
            user: req.user,
        })
    }
})

router.post( '/checkin', function ( req: express.Request, res: express.Response ) {
    console.log( 'here' );
    if ( req.user ) {
        console.log( 'here2' );
        console.log( "got this checkin body:" + req.body )
        var checkinCommand: SessionCommands.IStartSessionCommand = {
            accountID: req.user.id,
            latitude: req.body.latitude,
            longitude: req.body.longitude,
            createdDate: req.body.createdDate,
            sessionType: SessionEnums.SessionType.CheckIn,
            name: req.body.name,
        }

        req.SessionManager.startSession( checkinCommand, ( err, newsession ) => {
            if ( err ) {
                return res.send( err );
            }
            console.log( 'created session: ' + newsession );

            res.redirect( '/' );
        })
    }
    else {
        res.send( 'not authenticated' );
    }
})


router.get( '/steps', function ( req: express.Request, res: express.Response ) {
    if ( req.user ) {
        res.render( 'session_add_steps', {
            user: req.user
        })
    } else {
        res.redirect( '/' );
    }
})

router.post( '/steps', function ( req: express.Request, res: express.Response ) {
    if ( req.user ) {
        var stepsCommand: SessionCommands.IStartSessionCommand = {
            accountID: req.user.id,
            createdDate: req.body.createdDate,
            sessionType: SessionEnums.SessionType.WalkingRunning,
            totalSteps: req.body.steps,
            name: req.body.name,
        }

        req.SessionManager.startSession( stepsCommand, ( err, newsession ) => {
            if ( err ) {
                return res.send( err )
            }
            console.log( 'created session: ' + newsession );

            res.redirect( '/' );
        })
    } else {
        // TODO - return forbidden
        return null;
    }
})


export = router;


