/// <reference path="../references.ts" />

import express = require( 'express' );
import SessionManager = require( '../classes/sessions/SessionManager' );

var router = express();

//------------------
// List all accounts
//------------------
router.get( '/', function ( req: express.Request, res ) {
    req.AccountManager.listAllAccounts( function ( result ) {
        res.send( result );
    });
});


//------------------
// Get account
//------------------
router.get( '/:accountID', function ( req: express.Request, res ) {
    var query: AccountCommands.AccountQuery =
        {
            AccountID: req.params.accountID
        };

    req.AccountManager.getAccount( query, function ( result ) {
        res.send( result );
    });
});

//------------------
// Login
//------------------
router.post( '/login', function ( req: express.Request, res ) {
    var command: AccountCommands.LoginCommand = <AccountCommands.LoginCommand>req.body;
    console.info( "login with " + command.Email + " " + command.Password );

    req.AccountManager.login( command, function (err, account ) {
        if(err)
            return res.send(err);
            
        return res.send(account);
    });
});


//------------------
// Create account
//------------------
router.post( '/', function ( req: express.Request, res ) {
    var command: AccountCommands.CreateUserCommand = <AccountCommands.CreateUserCommand>req.body;
    req.AccountManager.createUser( command, function ( result ) {
        res.send( result );
    });
});


//------------------
// start session
//------------------
router.post( '/:accountID/sessions', function ( req: express.Request, res: express.Response ) {
    var command: SessionCommands.IStartSessionCommand = <SessionCommands.IStartSessionCommand>req.body;
    command.accountID = req.params.accountID;
    command.createdDate = new Date();
    req.SessionManager.startSession( command, ( err, result ) => {
        if ( err )
            return res.send( err );
        else
            return res.send( result );
    });
});

export = router;