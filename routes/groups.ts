/// <reference path="../references.ts" />

import express = require( 'express' );
import GroupManager = require( '../classes/groups/GroupManager' );

var router = express();

router.get( '/', function ( req: express.Request, res ) {
    if ( req.query.name ) {
        var groupQuery: GroupCommands.GroupQuery = {
            Name: req.query.name,
            Latitude: req.query.lat,
            Longitude: req.query.lng,
        }
        req.GroupManager.searchGroups( groupQuery, ( err, data ) => {
            if(err) {
                res.send(err);
            }
            console.log( "returning search results" );
            console.log( data );
            res.send(data);
        })
    }
    else {
        req.GroupManager.listAllGroups( (err, groups) => {
            if(err){
                res.send(err);
            }
                
            res.send( groups );
        });
    }
});

router.get( '/:groupID', function ( req: express.Request, res ) {
    var command: GroupCommands.GroupQuery = {
        GroupID: req.params.groupID,
    }

    req.GroupManager.getGroup( command, ( result ) => {
        res.send( result );
    });
});

router.post( '/search', function ( req: express.Request, res ) {
    var command: GroupCommands.GroupQuery = <GroupCommands.GroupQuery>req.body;
    req.GroupManager.searchGroups( command, ( result ) => {
        res.send( result );
    });
});

//------------------
// Join group
//------------------
router.put( '/:groupID/members/:accountID', function ( req: express.Request, res ) {
    var command: GroupCommands.JoinGroupCommand = <GroupCommands.JoinGroupCommand>req.body;
    command.AccountID = req.params.accountID;
    command.GroupID = req.params.groupID;

    req.GroupManager.joinGroup( command, function ( result ) {
        res.send( result );
    });
});

router.delete( '/:groupID/members/:accountID', function ( req: express.Request, res ) {
    var command: GroupCommands.LeaveGroupCommand = <GroupCommands.LeaveGroupCommand>req.body;
    command.AccountID = req.params.accountID;
    command.GroupID = req.params.groupID;

    req.GroupManager.leaveGroup( command, function ( result ) {
        res.send( result );
    });
});

//------------------
// Create Group
//------------------
router.post( '/', function ( req: express.Request, res ) {
    var command: GroupCommands.CreateGroupCommand = <GroupCommands.CreateGroupCommand>req.body;
    req.GroupManager.createGroup( command, ( result ) => {
        res.send( result );
    });
});

export = router;