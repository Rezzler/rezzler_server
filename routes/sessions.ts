/// <reference path="../references.ts" />

import express = require( 'express' );
import SessionManager = require( '../classes/sessions/SessionManager' );

var router = express();

//------------------
// List all sessions
//------------------
router.get( '/', function ( req: express.Request, res : express.Response ) {
    req.SessionManager.getSessions((err, sessions) => {
      if(err) { 
          return res.send(err);
      }
      
      return res.send(sessions);
    })
});



export = router;