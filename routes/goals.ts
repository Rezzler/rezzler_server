/// <reference path="../references.ts" />

import express = require( 'express' );
import GoalManager = require( '../classes/goals/GoalManager' );
import GoalCommon = require('../classes/goals/GoalCommon');

var router = express();

router.get( '/', function ( req: express.Request, res ) {
    req.GoalManager.listAllGoals( (err, goals) => {
        if(err)
            res.send(err)
            
        res.send( goals );
    });
});

router.get( '/:goalID', function ( req: express.Request, res ) {
    var command: GoalCommands.GoalQuery = { GoalId: req.params.goalID };

    req.GoalManager.getGoal( command, (err, goal : GoalCommon.IGoalInstance ) => {
        if(err)
            return res.send(err);
        
        //req.SessionManager.getSessionsForAccountSinceDate(goal.
        res.send( goal );
    });
});

router.post( '/search', function ( req: express.Request, res ) {
    var command: GoalCommands.GoalQuery = <GoalCommands.GoalQuery>req.body;

    req.GoalManager.searchGoals( command, ( result ) => {
        res.send( result );
    });
});

export = router;