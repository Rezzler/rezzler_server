import express = require('express');
var router = express();

router.get('/ping', function(req, res) {
    res.send({Status:'Running', Version:'0.01'});
});

export = router;