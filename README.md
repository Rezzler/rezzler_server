# Server Documentation #

Instead of punting this some other place or in a documentation I'm converting what I have finished in word doc form here.

[Models](https://bitbucket.org/recountable/Models)

[Routes](https://bitbucket.org/recountable/Routes)

[Classes](https://bitbucket.org/recountable/Classes)

### A few short, but easy examples ###
[API Examples](https://bitbucket.org/recountable/ApiExamples)